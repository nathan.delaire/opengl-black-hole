//
// Created by natha on 09/12/2021.
//

#include "Engine.hpp"

//============================== CONSTRUCTOR / DESTRUCTOR ==============================//


Engine::Engine(int width, int height, const char *title) :
        W_WIDTH(width), W_HEIGHT(height), title(title){

    window = nullptr;
    nearPlane = 0.1f;

    initGlfw();
    initWindow();
    initImGui();
    initGlew();
    initOpenGLOptions();

    initShaders();
    initCamera();
    initMatrices();


    //TODO : Remove this at some point
    //initFrame();

    initSkySphere();
    initBlackHole();
    initComputeUniforms();

    logo = new Texture("../img/title.png");

    std::ifstream f1(shaders[0]);
    if(!f1.good())
        shaders[0] += 3;
    std::ifstream f2(shaders[1]);
    if(!f2.good())
        shaders[1] += 3;

}

Engine::~Engine() {
    glfwDestroyWindow(window);
    glfwTerminate();
}

//============================== INIT ==============================//

void Engine::initGlfw() {
    if(glfwInit() == GLFW_FALSE) {
        std::cout << "ERROR::GLFW_INIT_FAILED" << std::endl;
        glfwTerminate();
    }
}

void Engine::framebuffer_resize_callback(GLFWwindow *window, int fbW, int fbH) {
    glViewport(0, 0, fbW, fbH);
}

void Engine::initWindow() {
    const int GLMajor = 4;
    const int GLMinor = 4;

    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, GLMajor);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, GLMinor);

    glfwWindowHint(GLFW_RESIZABLE, true);

    window = glfwCreateWindow(W_WIDTH, W_HEIGHT, title, NULL, NULL);

    if(!window)
    {
        std::cout << "ERROR::INIT_WINDOW_FAILED" << std::endl;
        glfwTerminate();
    }

    fbwidth = W_WIDTH;
    fbheight = W_HEIGHT;

    glfwGetFramebufferSize(window, &fbwidth, &fbheight);
    glfwSetFramebufferSizeCallback(window, framebuffer_resize_callback);

    glfwMakeContextCurrent(window);
}

void Engine::initImGui() {
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO &io = ImGui::GetIO();

    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init("#version 440");
    ImGui::StyleColorsDark();
}

void Engine::initGlew() {

    glewExperimental = GL_TRUE;
    if(glewInit() != GLEW_OK)
    {
        std::cout << "ERROR::GLEW_INIT_FAILED" << std::endl;
        glfwTerminate();
    }
}

void Engine::initOpenGLOptions() {
    //OpenGL Options
    glEnable(GL_DEPTH_TEST);

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    //Show cursor
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
}

void Engine::initShaders() {
    mainShader = new Shader("../shaders/vertex_core.glsl",
                            "../shaders/fragment_core.glsl");

    Shader::printWorkGroupCapabilities();


    computeShader = new ComputeShader(shaders[0]);

}

void Engine::initCamera() {
    camera = Camera(glm::vec3(-7.f, 2.f,1.f),
                    glm::vec3(1.f, 0.f, 0.f),
                    glm::vec3(0.f, 1.f, 0.f));
}

void Engine::initMatrices() {
    ProjectionMatrix = glm::perspective(glm::radians(camera.fov), static_cast<float>(fbwidth) / fbheight,
                                        nearPlane, 10000.f);
    ViewMatrix = camera.getViewMatrix();
    ModelMatrix = glm::translate(glm::mat4(), glm::vec3(0.f));
}

void Engine::initFrame() {

    glm::vec4 TL = glm::vec4(1.f , 1.0f , - nearPlane - 0.5f, 1.f) * ViewMatrix;
    glm::vec4 BL = glm::vec4(1.f , -1.0f , - nearPlane - 0.5f, 1.f) * ViewMatrix;
    glm::vec4 BR = glm::vec4(-1.f , -1.0f , - nearPlane - 0.5f, 1.f) * ViewMatrix;
    glm::vec4 TR = glm::vec4(-1.f , 1.0f , - nearPlane - 0.5f, 1.f) * ViewMatrix;

    float vertices[] = {
            //Position
            /*0*/ TL.x + camera.getPosition().x, TL.y + camera.getPosition().y, TL.z + camera.getPosition().z,
            /*1*/ BL.x + camera.getPosition().x, BL.y + camera.getPosition().y, BL.z + camera.getPosition().z,
            /*2*/ BR.x + camera.getPosition().x, BR.y + camera.getPosition().y, BR.z + camera.getPosition().z,
            /*3*/ TR.x + camera.getPosition().x, TR.y + camera.getPosition().y, TR.z + camera.getPosition().z,
    };

    unsigned nrOfVertices = sizeof(vertices) / sizeof(float);

    //Indexes of points that will be used for construction (avoid having two vertex at the same place)
    unsigned int indices[] = {
            0, 1, 3, //TR1
            1, 2, 3  //TR2
    };

    unsigned nrOfIndices = sizeof(indices) / sizeof(unsigned int);

    float texCoords[] = {
            1.f, 0.f,
            1.f, 1.f,
            0.f, 1.f,
            0.f, 0.f
    };

    unsigned nrOfTexcoords = sizeof(texCoords) / sizeof(float);

    frame = new Model(vertices,
                      nrOfVertices,
                      texCoords,
                      nrOfTexcoords,
                      indices,
                      nrOfIndices);

    frameQuad = { glm::vec3(TL.x + camera.getPosition().x, TL.y + camera.getPosition().y, TL.z + camera.getPosition().z),
                  glm::vec3(TR.x + camera.getPosition().x, TR.y + camera.getPosition().y, TR.z + camera.getPosition().z),
                  glm::vec3(BL.x + camera.getPosition().x, BL.y + camera.getPosition().y, BL.z + camera.getPosition().z),
                  glm::vec3(BR.x + camera.getPosition().x, BR.y + camera.getPosition().y, BR.z + camera.getPosition().z)};

    std::cout << "TL : " << frameQuad.topLeft.x << " " << frameQuad.topLeft.y << " " << frameQuad.topLeft.z << std::endl;
    std::cout << "TR : " << frameQuad.topRight.x << " " << frameQuad.topRight.y << " " << frameQuad.topRight.z << std::endl;
    std::cout << "BL : " << frameQuad.bottomLeft.x << " " << frameQuad.bottomLeft.y << " " << frameQuad.bottomLeft.z << std::endl;
    std::cout << "BR : " << frameQuad.bottomRight.x << " " << frameQuad.bottomRight.y << " " << frameQuad.bottomRight.z << std::endl;

}

void Engine::initSkySphere() {
    skySphere = new SkySphere(2000.f,
                              camera.getPosition(),
                              textureData[selected_background].first,
                              textureData[selected_background].second);
}

void Engine::initBlackHole() {
    blackHole = new BlackHole(1.3f,
                              3.8f,
                              glm::vec3(10, 0, 1),
                              "../img/disk_tex.png",
                              glm::vec2(1600, 1600));
}

void Engine::initComputeUniforms() {


    //Bind space texture
    computeShader->use();
    glActiveTexture(GL_TEXTURE1);
    skySphere->getSpaceTex()->bind();
    glActiveTexture(GL_TEXTURE2);
    blackHole->getAccDiskTex()->bind();
    glActiveTexture(GL_TEXTURE3);
    computeShader->unuse();

    computeShader->use();
    //Send skybox infos
    computeShader->setFloat("spaceBox.radius", skySphere->getRadius());
    computeShader->setVec3("spaceBox.center", skySphere->getCenter());
    computeShader->setTexture("spaceBox.texture", 1);
    computeShader->setVec2("spaceBox.texRes", skySphere->getTexRes());

    //Send black hole infos
    computeShader->setFloat("blackHole.radius", blackHole->getRadius());
    computeShader->setFloat("blackHole.ad_radius", blackHole->getAccretionDiskRadius());
    computeShader->setVec3("blackHole.position", blackHole->getPosition());
    computeShader->setTexture("blackHole.ad_texture", 2);
    computeShader->setVec2("blackHole.ad_textureRes", blackHole->getAccDiskTesRes());
    computeShader->unuse();
}


//============================== UPDATE ==============================//

void Engine::update() {
    /**
     * 1 - Update View and Projection Matrices according to mouse and keyboard
     * 2 - Update frame corners positions according to View Matrix
     * 3 - Update compute uniforms (frame corners)
     * 4 - Run compute (raytracing)
     * 5 - Update texture bindings for core
     * 6 - Update core uniforms (matrix + texture?)
     */

    glfwPollEvents();
    updateDeltaTime();
    updateImGui();
    if(selected_background != previous_background){
        skySphere->updateTexture(textureData[selected_background].first, textureData[selected_background].second);
        computeShader->use();
        glActiveTexture(GL_TEXTURE1);
        skySphere->getSpaceTex()->bind();
        computeShader->unuse();
        previous_background = selected_background;
    }
    if(current_shader != previous_shader){
        delete computeShader;
        computeShader = new ComputeShader(shaders[current_shader]);
        initComputeUniforms();
        previous_shader = current_shader;
    }

    updateKeyboardInput();
    if(!camera.locked)
        updateMouseInput();
    updateMatrices();
    updateFrame();
    updateComputeUniforms();
    runComputeShader();
    updateTextureBindings();
    updateCoreUniforms();
    if(!camera.locked)
        camera.updateInput(dt, -1, mouseOffsetX, mouseOffsetY);
}

void Engine::updateDeltaTime() {
    curTime = static_cast<float>(glfwGetTime());
    dt = curTime - lastTime;
    lastTime = curTime;
}


void Engine::updateKeyboardInput() {
    //Quit
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);

    //Camera
    if(glfwGetKey(window , GLFW_KEY_W) == GLFW_PRESS)
        camera.updateKeyboardInput(dt, FORWARD);
    if(glfwGetKey(window , GLFW_KEY_A) == GLFW_PRESS)
        camera.updateKeyboardInput(dt, LEFT);
    if(glfwGetKey(window , GLFW_KEY_D) == GLFW_PRESS)
        camera.updateKeyboardInput(dt, RIGHT);
    if(glfwGetKey(window , GLFW_KEY_S) == GLFW_PRESS)
        camera.updateKeyboardInput(dt, BACKWARD);
    if(glfwGetKey(window , GLFW_KEY_SPACE) == GLFW_PRESS)
        camera.updateKeyboardInput(dt, UP);
    if(glfwGetKey(window , GLFW_KEY_C) == GLFW_PRESS)
        camera.updateKeyboardInput(dt, DOWN);
}

void Engine::updateMouseInput() {
    glfwGetCursorPos(window, &mouseX, &mouseY);

    if(firstMouse)
    {
        lastMouseX = mouseX;
        lastMouseY = mouseY;
        firstMouse = false;
    }

    mouseOffsetX = mouseX - lastMouseX;
    mouseOffsetY = mouseY - lastMouseY;

    lastMouseX = mouseX;
    lastMouseY = mouseY;
}

void Engine::updateMatrices() {
    //Get new width and height from window in case of window size changing
    glfwGetFramebufferSize(window, &fbwidth, &fbheight);

    //Camera automatic rotation if locked
    if(camera.locked){
        float angle = (3.14f / 128) * dt;
        float new_x = cos(angle) * (camera.getPosition().x - blackHole->getPosition().x) - sin(angle) * (camera.getPosition().z - blackHole->getPosition().z) + blackHole->getPosition().x;
        float new_z = sin(angle) * (camera.getPosition().x - blackHole->getPosition().x) + cos(angle) * (camera.getPosition().z - blackHole->getPosition().z) + blackHole->getPosition().z;
        camera.setPosition(glm::vec3(new_x, camera.getPosition().y, new_z));

        camera.setFront(blackHole->getPosition() - camera.getPosition());
    }

    //Update matrices translations -> Things that change every frame
    ViewMatrix = camera.getViewMatrix();
    /**
    std::cout << ViewMatrix[0].x << " " << ViewMatrix[0].y << " " << ViewMatrix[0].z << " " << ViewMatrix[0].w << std::endl;
    std::cout << ViewMatrix[1].x << " " << ViewMatrix[1].y << " " << ViewMatrix[1].z << " " << ViewMatrix[1].w << std::endl;
    std::cout << ViewMatrix[2].x << " " << ViewMatrix[2].y << " " << ViewMatrix[2].z << " " << ViewMatrix[2].w << std::endl;
    std::cout << ViewMatrix[3].x << " " << ViewMatrix[3].y << " " << ViewMatrix[3].z << " " << ViewMatrix[3].w << std::endl;
    std::cout << std::endl;
    **/
    ProjectionMatrix = glm::perspective(glm::radians(camera.fov),
                                        static_cast<float>(fbwidth) / fbheight,
                                        0.1f, 100000.f);
}

void Engine::updateFrame() {

    if(!frame)
        delete frame;

    glm::vec4 TL = glm::vec4(-1.f , 1.0f , - nearPlane - 0.5f, 1.f) * ViewMatrix;
    glm::vec4 TR = glm::vec4(1.f , 1.0f , - nearPlane - 0.5f, 1.f) * ViewMatrix;
    glm::vec4 BR = glm::vec4(1.f , -1.0f , - nearPlane - 0.5f, 1.f) * ViewMatrix;
    glm::vec4 BL = glm::vec4(-1.f , -1.0f , - nearPlane - 0.5f, 1.f) * ViewMatrix;

    float vertices[] = {
            //Position
            /*0*/ TL.x + camera.getPosition().x, TL.y + camera.getPosition().y, TL.z + camera.getPosition().z,
            /*1*/ BL.x + camera.getPosition().x, BL.y + camera.getPosition().y, BL.z + camera.getPosition().z,
            /*2*/ BR.x + camera.getPosition().x, BR.y + camera.getPosition().y, BR.z + camera.getPosition().z,
            /*3*/ TR.x + camera.getPosition().x, TR.y + camera.getPosition().y, TR.z + camera.getPosition().z,
    };

    unsigned nrOfVertices = sizeof(vertices) / sizeof(float);

    //Indexes of points that will be used for construction (avoid having two vertex at the same place)
    unsigned int indices[] = {
            0, 3, 1, //TR1
            3, 1, 2  //TR2
    };

    unsigned nrOfIndices = sizeof(indices) / sizeof(unsigned int);

    float texCoords[] = {
            1.f, 0.f,
            1.f, 1.f,
            0.f, 1.f,
            0.f, 0.f
    };

    unsigned nrOfTexcoords = sizeof(texCoords) / sizeof(float);

    frame = new Model(vertices,
                      nrOfVertices,
                      texCoords,
                      nrOfTexcoords,
                      indices,
                      nrOfIndices);

    frameQuad = { glm::vec3(TL.x + camera.getPosition().x, TL.y + camera.getPosition().y, TL.z + camera.getPosition().z),
                  glm::vec3(TR.x + camera.getPosition().x, TR.y + camera.getPosition().y, TR.z + camera.getPosition().z),
                  glm::vec3(BL.x + camera.getPosition().x, BL.y + camera.getPosition().y, BL.z + camera.getPosition().z),
                  glm::vec3(BR.x + camera.getPosition().x, BR.y + camera.getPosition().y, BR.z + camera.getPosition().z)};

    /**
    std::cout << frameQuad.topLeft.x << " " << frameQuad.topLeft.y << " " << frameQuad.topLeft.z << std::endl;
    std::cout << frameQuad.topRight.x << " " << frameQuad.topRight.y << " " << frameQuad.topRight.z << std::endl;
    std::cout << frameQuad.bottomLeft.x << " " << frameQuad.bottomLeft.y << " " << frameQuad.bottomLeft.z << std::endl;
    std::cout << frameQuad.bottomRight.x << " " << frameQuad.bottomRight.y << " " << frameQuad.bottomRight.z << std::endl;
    std::cout << std::endl;
    **/
}

void Engine::updateComputeUniforms() {
    computeShader->use();
    computeShader->setVec3("topLeft", frameQuad.topLeft);
    computeShader->setVec3("topRight", frameQuad.topRight);
    computeShader->setVec3("bottomLeft", frameQuad.bottomLeft);
    computeShader->setVec3("camPos", camera.getPosition());

    computeShader->setVec3("blackHole.position", blackHole->getPosition());
    computeShader->setInt("blackHole.is_event_h", blackHole->with_event_horizion);
    computeShader->setInt("blackHole.is_distortion", blackHole->with_distortion);
    computeShader->setInt("blackHole.is_accretion", blackHole->with_accretion_disk);
    computeShader->setInt("blackHole.is_doppler", blackHole->with_doppler_effect);


    computeShader->setFloat("t", curTime);
    computeShader->unuse();
}

void Engine::runComputeShader() {
    computeShader->run();
}

void Engine::updateTextureBindings() {
    mainShader->use();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, computeShader->rendertextureID);
    mainShader->unuse();
}

void Engine::updateCoreUniforms() {

    mainShader->use();
    mainShader->setMat4("projection", ProjectionMatrix);
    mainShader->setMat4("view", ViewMatrix);
    mainShader->setTexture("main_tex", 0);
    mainShader->unuse();
}

void Engine::updateImGui() {
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
    ImGui::SetNextWindowSize(ImVec2(405, 800));

    ImGui::Begin("Menu");
    ImGui::Image((void*)(intptr_t)logo->getID(), ImVec2(400, 220));

    if(ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen)){
        int locked_value = (int)camera.locked;
        ImGui::RadioButton("Locked around BlackHole", &locked_value, 1);
        ImGui::SameLine();
        ImGui::RadioButton("Free", &locked_value, 0);
        if(locked_value == 1) {
            camera.locked = true;
            mouseOffsetX = 0;
            mouseOffsetY = 0;
        }
        else {
            camera.locked = false;
        }
    }

    if(ImGui::CollapsingHeader("Shader", ImGuiTreeNodeFlags_DefaultOpen)){
       ImGui::RadioButton("Simple precision (fast rendering)", &current_shader, 0);
       ImGui::RadioButton("Double precision (better but slower rendering)", &current_shader, 1);
    }

    if(ImGui::CollapsingHeader("Background", ImGuiTreeNodeFlags_DefaultOpen)){
        ImGui::ListBox("", &selected_background, textureNames.data(), textureNames.size());
    }

    if(ImGui::CollapsingHeader("Black Hole Options", ImGuiTreeNodeFlags_DefaultOpen)){
        ImGui::Checkbox("Event Horizion", &blackHole->with_event_horizion);
        ImGui::Checkbox("Gravitational Lensing", &blackHole->with_distortion);
        ImGui::Checkbox("Accretion Disk", &blackHole->with_accretion_disk);
        ImGui::Checkbox("Doppler Effect", &blackHole->with_doppler_effect);
    }

    if(ImGui::CollapsingHeader("Controls"), ImGuiTreeNodeFlags_DefaultOpen){
        ImGui::Text("Z : Move forward");
        ImGui::Text("Q : Move Left");
        ImGui::Text("D : Move Right");
        ImGui::Text("S : Move Backward");
        ImGui::Text("Space : Move Up");
        ImGui::Text("C : Move Down");
        ImGui::Text("Esc : Quit");
    }

    if(ImGui::CollapsingHeader("Informations", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
        ImGui::Text("Project made by Nathan DELAIRE and Matthieu BOLLIAND");
        ImGui::Text("December 2021 - EPITA - POGLA");
    }

    ImGui::End();
}

//============================== RENDER ==============================//


void Engine::render() {
    glViewport(0, 0, fbwidth, fbheight);
    glClearColor(0.4f, 0.4f, 0.4f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    frame->render(mainShader);

    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    glfwSwapBuffers(window);
    glFlush();
}

//========================== OTHER =======================//

GLFWwindow *Engine::getWindow() const {
    return window;
}
#version 440

in vec2 tc;

//Texture
uniform sampler2D main_tex;

out vec4 fragColor;

void main(){

    vec4 tex_point = texture(main_tex, tc);

    fragColor = vec4(tex_point.x, tex_point.y, tex_point.z, 1.f);
}
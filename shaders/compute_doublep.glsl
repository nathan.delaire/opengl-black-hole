#version 440

#define PI 3.141592
#define G 0.0043 // in parsec * (solar mass)^-1 * (km/s)^2
#define C 299792 // in (km/s)^2

//Nb pixels par carrés
layout (local_size_x = 32, local_size_y = 32) in;

layout (rgba32f, binding = 0) uniform image2D img_output;

//SkySphere for space background
struct SpaceBox
{
    float radius;
    vec3 center;//Center of black hole
    sampler2D texture;
    vec2 texRes; //Resolution of texture
};

//Black hole
struct BlackHole
{
    float mass;
    float radius;
    float ad_radius;
    sampler2D ad_texture; //Accretion Disk texture
    vec2 ad_textureRes;
    vec3 position;
};

//Corners of frame
uniform vec3 topLeft;
uniform vec3 topRight;
uniform vec3 bottomLeft;

//Camera position
uniform vec3 camPos;

//Space background
uniform SpaceBox spaceBox;

//Black hole object
uniform BlackHole blackHole;

//Time
uniform float t;

//Cast single ray from point p with direction to see where it intersects spaceBox
dvec3 getPointSpaceBox(const dvec3 p, const dvec3 direction){
    dvec3 oc = p - spaceBox.center;
    double a = dot(direction, direction);
    double b = 2.0 * dot(oc, direction);
    double c = dot(oc,oc) - spaceBox.radius * spaceBox.radius;
    double discriminant = b*b - 4*a*c;
    double t1 = (-b - sqrt(discriminant)) / 2 * a;

    return p + t1 * direction;
}

//Sky Texture Mapping using spherical coordinates
vec4 skyTextureMapping(dvec3 pt_sb){

    //Transform into spherical coordinates
    vec3 pt_sbf = vec3(float(pt_sb.x), float(pt_sb.y), float(pt_sb.z));

    float phi = acos(pt_sbf.x / sqrt(pow(pt_sbf.x, 2.f) + pow(pt_sbf.y, 2.f) + pow(pt_sbf.z, 2.f)));
    float theta = atan(pt_sbf.y, pt_sbf.z);
    //Get texture coordinates
    float spacetex_x = mod( (phi / (2 * PI)) , spaceBox.texRes.x);
    float spacetex_y = mod( (theta / PI) , spaceBox.texRes.y);
    //Get value on texture
    vec4 texPoint = texture(spaceBox.texture, vec2(spacetex_x * 8, spacetex_y * 8));

    return texPoint;
}


float sphereDistanceEstimator(dvec3 p, vec3 center, float radius)
{
    return float(length(p - center) - radius);
}

float cylinderDistanceEstimator(dvec3 p, vec3 center, float h, float r )
{
    p = p - center;
    vec2 d = abs(vec2(length(p.xz),p.y)) - vec2(h,r);
    return min(max(d.x,d.y),0.0) + length(max(d,0.0));
}


double GetSpaceDistortionLerpValue( float schwarzschildRadius,
                                    double distanceToSingularity,
                                    float spaceDistortion) {
    return pow(schwarzschildRadius, spaceDistortion)
    / pow(float(distanceToSingularity), spaceDistortion);
}

//from = cameraPosition
//direction = vector between cameraPosition and point on frame
//Raymarcher + Texture sampler
vec4 getPixelColor(vec3 from, dvec3 direction)
{
    float totalDistance = 0.f;
    int steps = 0;
    int MAX_RAY_STEPS = 21;

    //initialize point at camPos
    dvec3 p_bh = from;
    dvec3 p_ad = from;

    bool in_bh = false;

    //Direction will change because of the gravitational lensing effect
    //Each steps is when we move on the ray
    for(; steps < MAX_RAY_STEPS; steps++){

        //Distance estimator estimates how far we have to advance the ray
        float distance = sphereDistanceEstimator(p_bh, blackHole.position, blackHole.radius - steps / 5);

        //Distance estimator from accrection disk
        float dist_ad = cylinderDistanceEstimator(p_ad, blackHole.position, blackHole.radius + blackHole.ad_radius, 0.05f);

        //Current point according to black hole raymarcher
        p_bh = p_bh + distance * direction;

        //Current point according to accretion disk raymarcher
        p_ad = p_ad + dist_ad * direction;

        //Distance to current point from black hole
       dvec3 point_to_BH = blackHole.position - p_bh;

        //If interception with accretion disk
        if(dist_ad < 0.15f)
        {
            //Doppler Effect
            vec4 doppler_value = vec4(0.4f, 1.f, 1.f, 1.f) * (1 / pow(float(length(p_ad - blackHole.position)), 0.6f));


            //Rotation
            float angle = PI * t;
            double new_x = cos(angle) * (p_ad.x - blackHole.position.x) - sin(angle) * (p_ad.z - blackHole.position.z) + blackHole.position.x;
            double new_z = sin(angle) * (p_ad.x - blackHole.position.x) + cos(angle) * (p_ad.z - blackHole.position.z) + blackHole.position.z;
            p_ad = dvec3(new_x, 0, new_z);

            double ad_tex_x = p_ad.z - blackHole.position.z + 6;
            double ad_tex_y = p_ad.x - blackHole.position.x + 6;

            vec2 adTex = vec2(ad_tex_x / 12, ad_tex_y / 12);

            vec4 ad_color = texture(blackHole.ad_texture, adTex) + vec4(0, 0.4, 0, 1) - float(length(p_ad - blackHole.position) / 15);
            ad_color += doppler_value;

            //Cast ray from current point with distorted direction into spaceBox
            dvec3 pt_sb = getPointSpaceBox(p_bh, direction);
            vec4 sb_color = skyTextureMapping(pt_sb);

            float ad_value = ad_color.x;
            return mix(sb_color, ad_color, ad_value);

        }

        //If interception with black hole
        if(distance < 0.3f){
            return vec4(0.f, 0.f, 0.f, 1.f);
        }


        //Get value of distortion
        double lerp_value = GetSpaceDistortionLerpValue(0.5f, length(point_to_BH), 2.f);

        //Change value according to distortion
        direction = normalize(direction * (1. - lerp_value) + point_to_BH * lerp_value);

    }


    //Cast ray from current point with distorted direction into spaceBox
    dvec3 pt_sb = getPointSpaceBox(p_bh, direction);

    return skyTextureMapping(pt_sb);
}




void main() {
    // Aucun tableau de donnée n'étant passé au moment de la création de la texture,
    // c'est le compute shader qui va dessiner à l'intérieur de l'image associé
    // à la texture.

    // gl_LocalInvocationID.xy * gl_WorkGroupID.xy == gl_GlobalInvocationID
    ivec2 coords = ivec2(gl_GlobalInvocationID);


    // Pour mettre en evidence. Les groupes de travail locaux on dessine un damier.


    // ===================== GET COORDINATE OF POINT IN FRAME =============//


    //First we get topLeft topRight vector in order to go along
    //the vector to find point in x axis
    vec3 TLTR = topRight - topLeft;
    //Then we get offset from where to get point in x axis (1920 because frame is 1920x1920)
    vec3 offset_x = (TLTR / 1920) * coords.x;

    //We do the same with topLeft bottomLeft for y axis
    vec3 TLBL = bottomLeft - topLeft;
    vec3 offset_y = (TLBL / 1920) * coords.y;

    //We get final point in frameQuad from where cast ray
    vec3 framePoint = topLeft + offset_x + offset_y;


    // ============ RAY MARCHING ============//

    vec3 direction = framePoint - camPos;
    //Main Function (Raymarch + Texture sampling)
    vec4 pixel = getPixelColor(camPos, direction);

    imageStore(img_output, coords, pixel);
}
//
// Created by natha on 09/12/2021.
//

#ifndef BLACK_HOLE_COMPUTESHADER_HPP
#define BLACK_HOLE_COMPUTESHADER_HPP

#include "Shader.hpp"

class ComputeShader : public Shader {

public:
    explicit ComputeShader(const char *cFile);

    void run();
    GLuint rendertextureID = 0;

private:
    void initRenderTexture();
};


#endif //BLACK_HOLE_COMPUTESHADER_HPP

//
// Created by natha on 09/12/2021.
//

#ifndef BLACK_HOLE_SHADER_HPP
#define BLACK_HOLE_SHADER_HPP

#include "../libs.hpp"

class Shader {

public:

    Shader(const char* vFile,
           const char* fFile,
           const char* cFile = nullptr);

    ~Shader();

    void use() const;
    void unuse() const;

    // Utility uniform functions
    void setFloat(const std::string &name, float value) const {
        glUniform1f(glGetUniformLocation(id, name.c_str()), value);
    }
    void setDouble(const std::string &name, double value) const{
        glUniform1d(glGetUniformLocation(id, name.c_str()), value);
    }
    void setInt(const std::string &name, int value) const{
        glUniform1i(glGetUniformLocation(id, name.c_str()), value);
    }
    void setVec2(const std::string &name, glm::vec2 vec) const {
        glUniform2f(glGetUniformLocation(id, name.c_str()), vec.x, vec.y);
    }
    void setVec3(const std::string &name, glm::vec3 vec) {
        glUniform3fv(glGetUniformLocation(id, name.c_str()), 1, glm::value_ptr(vec));
    }
    void setMat4(const std::string &name, glm::mat4 mat) const {
        glUniformMatrix4fv(glGetUniformLocation(id, name.c_str()), 1, GL_FALSE, glm::value_ptr(mat));
    }
    void setTexture(const std::string &name, unsigned int textureLocation) {
        glUniform1i(glGetUniformLocation(id, name.c_str()), textureLocation);
    }

    GLuint id;

    static void printWorkGroupCapabilities(){
        int workgroup_count[3];
        int workgroup_size[3];
        int workgroup_invocations;

        glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 0, &workgroup_count[0]);
        glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 1, &workgroup_count[1]);
        glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 2, &workgroup_count[2]);

        printf ("Taille maximale des workgroups:\n\tx:%u\n\ty:%u\n\tz:%u\n",
                workgroup_count[0], workgroup_count[1], workgroup_count[2]);

        glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 0, &workgroup_size[0]);
        glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 1, &workgroup_size[1]);
        glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 2, &workgroup_size[2]);

        printf ("Nombre maximal d'invocation locale:\n\tx:%u\n\ty:%u\n\tz:%u\n",
                workgroup_size[0], workgroup_size[1], workgroup_size[2]);

        glGetIntegerv (GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS, &workgroup_invocations);
        printf ("Nombre maximum d'invocation de workgroups:\n\t%u\n", workgroup_invocations);
    }

protected:

    std::string loadShaderSource(const char* filename);

    GLuint loadShader(GLenum type, const char* filename);

    void linkProgram(GLuint vShader,
                     GLuint fShader,
                     GLuint cShader = 0);
};


#endif //BLACK_HOLE_SHADER_HPP

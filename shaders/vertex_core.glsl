#version 440

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 texCoords;

uniform mat4 projection;
uniform mat4 view;

out vec2 tc;

void main(){

    vec4 pos = projection * view * vec4(position, 1.0f);
    tc = texCoords;
    gl_Position = pos.xyzw;
}

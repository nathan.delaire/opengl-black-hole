//
// Created by natha on 09/12/2021.
//

#ifndef BLACK_HOLE_LIBS_HPP
#define BLACK_HOLE_LIBS_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <math.h>
#include <tuple>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "utils/stb_image.h"
#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw.h"
#include "imgui/imgui_impl_opengl3.h"


#endif //BLACK_HOLE_LIBS_HPP

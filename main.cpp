#include "Engine.hpp"

int main() {

    Engine engine(1920, 1080, "Black Hole");

    while (!glfwWindowShouldClose(engine.getWindow()))
    {
        engine.update();
        engine.render();
    }

    return 0;
}

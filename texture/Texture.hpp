//
// Created by natha on 09/12/2021.
//

#ifndef BLACK_HOLE_TEXTURE_HPP
#define BLACK_HOLE_TEXTURE_HPP

#include "../libs.hpp"

static std::vector<const char *> textureNames = {"Universe 1",
                                                 "Universe 2",
                                                 "Universe 3"};

static std::vector<std::pair<const char*, glm::vec2>> textureData = {std::make_pair("../img/universe1.png", glm::vec2(2000, 1847)),
                                                                      std::make_pair("../img/universe2.png", glm::vec2(1788, 2000)),
                                                                      std::make_pair("../img/universe3.png", glm::vec2(2000, 1972))};



class Texture {
public:

    Texture(std::string filename);
    Texture(std::string filename, std::vector<float> tc);

    void bind();
    void unbind();

    unsigned int getWidth() const { return w; }
    unsigned int getHeight() const { return h; }
    unsigned int getID() const { return textureID; }

    std::string getFilePath() const { return filepath; }

    GLvoid* getImageData();

    GLfloat texCoords[8];

private:

    std::string &filepath;
    unsigned int textureID;
    unsigned int w, h; //normalized
    unsigned int x, y; //coordinates normalized

    unsigned int load();
};


#endif //BLACK_HOLE_TEXTURE_HPP

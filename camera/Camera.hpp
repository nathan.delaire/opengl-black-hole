//
// Created by natha on 09/12/2021.
//

#ifndef BLACK_HOLE_CAMERA_HPP
#define BLACK_HOLE_CAMERA_HPP

#include "../libs.hpp"

enum directions{
    FORWARD = 0,
    BACKWARD = 1,
    LEFT = 2,
    RIGHT = 3,
    UP = 4,
    DOWN = 5
};

class Camera {
public:

    Camera();

    Camera(glm::vec3 position,
           glm::vec3 direction,
           glm::vec3 worldUp);

    virtual ~Camera();

    void updateKeyboardInput(const float &dt,
                             int direction);

    void updateMouseInput(const float &dt,
                          const double &offsetX,
                          const double &offsetY);

    /**
     * UpdateKeyboardInput and UpdateMouseInput
     */
    void updateInput(const float &dt,
                     int direction,
                     const double &offsetX,
                     const double &offsetY);

    //Getters
    glm::mat4 getViewMatrix();
    const glm::vec3 &getPosition() const;
    const glm::vec3 &getFront() const;
    const glm::vec3 &getCamUp() const;
    const glm::vec3 &getRight() const;
    GLfloat getPitch() const;
    GLfloat getYaw() const;

    //Setters
    void setPosition(const glm::vec3 &position);
    void setFront(const glm::vec3 &front);
    void setCamUp(const glm::vec3 &camUp);
    void setRight(const glm::vec3 &right);
    void setPitch(GLfloat pitch);
    void setYaw(GLfloat yaw);

    float fov;
    //Locked around black hole or not
    bool locked;
    bool notlocked;

private:

    void updateCameraVectors();

    //Matrix of camera where we do all our translation calculus
    glm::mat4 ViewMatrix;
    //Position vectors around camera
    glm::vec3 position;
    glm::vec3 front;
    //WorldUp -> Vecteur up du monde
    //camUp -> Vecteur up de la caméra sur le moment
    glm::vec3 worldUp;
    glm::vec3 camUp;
    glm::vec3 right;

    //Rotation sur x
    GLfloat pitch;
    //Rotation sur y
    GLfloat yaw;
    //Rotation sur z
    GLfloat roll;

    //For keyboard and mouse inputs
    GLfloat movement_speed;
    GLfloat sensitivity;
};


#endif //BLACK_HOLE_CAMERA_HPP

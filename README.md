# OpenGL Raymarched Black Hole

A simple OpenGL real-time engine displaying a Black Hole and its gravitational lensing effects, rendered using a Raymarcher.
Made by Nathan DELAIRE and Matthieu BOLLIAND.

Link for the demo : https://youtu.be/0bVfuzNcb8s

Link for the technical presentation (in French) : https://youtu.be/JibeJyzg6fQ

GitLab project : https://gitlab.com/nathan.delaire/opengl-black-hole

# Requirements
In order to run de projet, you need to have :
* Cmake (for Windows and Linux)
* OpenGL (minimum version 4.4)
* GLFW 3
* GLM
* GTC (Usually installed with GLM)

# Installation

## Windows
I personnaly use MinGW in order to have a C and C++ compiler on my Windows 10 machine. So the option -G in the line will be "MinGW Makefiles".
Go in the projet folder `opengl-black-hole` then :

``
cmake  -DCMAKE_BUILD_TYPE=Release -G "CodeBlocks - MinGW Makefiles" .
``

``
cmake --build . --target black_hole -- -j 12
``

A **black_hole.exe** will appear on your current folder. It's the application.
There is already one in the black_hole folder that you can use.

## Linux

Same lines as for Windows, as long as you of course change the G option.
It's a simple cmake project building, you don't need to put any particular variables or go in a particular file.

# How to use it


## Controls

Use your keyboard and your mouse to move around the world (here the keyboard is in AZERTY):
* **Z** : Front
* **S** : Back
* **Q** : Left
* **D** : Right
* **Space** : Up
* **C** : Down
* **Esc** : Quit application

## User Interface

When you first open the application, you'll have a user interface that will appear in front of you next to the black hole :

![](./img/ui.PNG)

In this interface, you will be able to :
* Change the behaviour of the camera (free movement or centered around black hole)
* Change the precision of the rendering (simple using `floats` and double using `double` variables)
* Change the background universe
* Display or not physical effects concerning the black hole

## Preview

![](./img/preview.PNG)


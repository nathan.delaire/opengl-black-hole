//
// Created by natha on 13/12/2021.
//

#include "BlackHole.hpp"

BlackHole::BlackHole(float radius, float accretionDiskRadius, const glm::vec3 &position,
                     const char *accDiskTexFile, const glm::vec2 &accDiskTexRes) : radius(radius),
                                                                                   accretion_disk_radius(accretionDiskRadius),
                                                                                   position(position),
                                                                                   accDiskTesRes(accDiskTexRes){
    accDiskTex = new Texture(accDiskTexFile);
    with_accretion_disk = true;
    with_distortion = true;
    with_doppler_effect = true;
    with_event_horizion = true;

}

float BlackHole::getRadius() const {
    return radius;
}

float BlackHole::getAccretionDiskRadius() const {
    return accretion_disk_radius;
}

const glm::vec3 &BlackHole::getPosition() const {
    return position;
}

Texture *BlackHole::getAccDiskTex() const {
    return accDiskTex;
}

const glm::vec2 &BlackHole::getAccDiskTesRes() const {
    return accDiskTesRes;
}

//
// Created by natha on 12/12/2021.
//

#ifndef BLACK_HOLE_SKYSPHERE_HPP
#define BLACK_HOLE_SKYSPHERE_HPP

#include "../texture/Texture.hpp"

class SkySphere {
public:
    SkySphere(float radius, const glm::vec3 &center, const char* texFile, glm::vec2 texRes);

    Texture *getSpaceTex() const;

    const glm::vec2 &getTexRes() const;

    float getRadius() const;

    const glm::vec3 &getCenter() const;

    void updateTexture(const char* texFile, glm::vec2 texRes);

private:

    //Space texture
    Texture *spaceTex;
    //Texture resolution
    glm::vec2 texRes;
    //Radius of skyphere
    float radius;
    //Center of skysphere
    glm::vec3 center;
};


#endif //BLACK_HOLE_SKYSPHERE_HPP

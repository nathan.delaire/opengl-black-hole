//
// Created by natha on 13/12/2021.
//

#ifndef BLACK_HOLE_BLACKHOLE_HPP
#define BLACK_HOLE_BLACKHOLE_HPP

#include "../libs.hpp"
#include "../texture/Texture.hpp"

class BlackHole {
public:
    BlackHole(float radius, float accretionDiskRadius, const glm::vec3 &position,
              const char *accDiskTexFile, const glm::vec2 &accDiskTexRes);

    float getRadius() const;

    float getAccretionDiskRadius() const;

    const glm::vec3 &getPosition() const;

    Texture *getAccDiskTex() const;

    const glm::vec2 &getAccDiskTesRes() const;

    glm::vec3 position;

    bool with_accretion_disk;
    bool with_doppler_effect;
    bool with_distortion;
    bool with_event_horizion;

private:

    float radius;
    float accretion_disk_radius;
    Texture *accDiskTex;
    glm::vec2 accDiskTesRes;

};


#endif //BLACK_HOLE_BLACKHOLE_HPP

//
// Created by natha on 12/12/2021.
//

#include "SkySphere.hpp"

SkySphere::SkySphere(float radius, const glm::vec3 &center, const char* texFile, glm::vec2 texRes) :
radius(radius), center(center), texRes(texRes) {
    spaceTex = new Texture(texFile);

}

Texture *SkySphere::getSpaceTex() const {
    return spaceTex;
}

float SkySphere::getRadius() const {
    return radius;
}

const glm::vec3 &SkySphere::getCenter() const {
    return center;
}

const glm::vec2 &SkySphere::getTexRes() const {
    return texRes;
}

void SkySphere::updateTexture(const char *texFile, glm::vec2 texRes) {
    spaceTex = new Texture(texFile);
    this->texRes = texRes;
}


//
// Created by Pierre on 12/07/2021.
//

#include "Model.hpp"

Model::Model(const float *vertices, unsigned int nbVertices, const float *textCoords, unsigned int nbTextCoords,
             const unsigned int *indices, unsigned int nbIndices) : vertices(vertices), nbVertices(nbVertices),
                                                              textCoords(textCoords), nbTextCoords(nbTextCoords),
                                                              indices(indices), nbIndices(nbIndices) {
    initVAO();
}


Model::~Model() {
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO_pixel);
    glDeleteBuffers(1, &VBO_tex);
    glDeleteBuffers(1, &EBO);
}


void Model::initVAO() {

    //VAO
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    //Gen Buffers
    glGenBuffers(1, &VBO_pixel);
    glGenBuffers(1, &VBO_tex);
    glGenBuffers(1, &EBO);

    //EBO
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, nbIndices * sizeof(unsigned int), indices, GL_STATIC_DRAW);

    //Positions (VBO_pixel)
    glBindBuffer(GL_ARRAY_BUFFER, VBO_pixel);
    glBufferData(GL_ARRAY_BUFFER, nbVertices * sizeof(float), vertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //Textures (VBO_tex)
    glBindBuffer(GL_ARRAY_BUFFER, VBO_tex);
    glBufferData(GL_ARRAY_BUFFER, nbTextCoords * sizeof(float), textCoords, GL_STATIC_DRAW);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);
}

void Model::render(Shader *shader) {
    //Bind VAO
    shader->use();
    glBindVertexArray(VAO);
    //Draw indices (triangles, nr of indices, type of indices, where do we start)
    if(nbIndices == 0)
        glDrawArrays(GL_TRIANGLES, 0, nbVertices);
    else
        glDrawElements(GL_TRIANGLES, nbIndices, GL_UNSIGNED_INT, 0);
    shader->unuse();
}


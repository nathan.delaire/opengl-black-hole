//
// Created by natha on 11/12/2021.
//

#ifndef BLACK_HOLE_FRAME_HPP
#define BLACK_HOLE_FRAME_HPP

#include "../libs.hpp"

struct Frame {
    glm::vec3 topLeft;
    glm::vec3 topRight;
    glm::vec3 bottomLeft;
    glm::vec3 bottomRight;
};

#endif //BLACK_HOLE_FRAME_HPP

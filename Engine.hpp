//
// Created by natha on 09/12/2021.
//

#ifndef BLACK_HOLE_ENGINE_HPP
#define BLACK_HOLE_ENGINE_HPP

#include "libs.hpp"
#include "shaders/Shader.hpp"
#include "shaders/ComputeShader.hpp"
#include "texture/Texture.hpp"
#include "camera/Camera.hpp"
#include "models/Model.hpp"
#include "models/Frame.hpp"
#include "models/SkySphere.hpp"
#include "models/BlackHole.hpp"


class Engine {

public:
    Engine(int width, int height, const char* title);
    virtual ~Engine();

    void update();
    void render();

    //variables initialization
    void initGlfw();
    static void framebuffer_resize_callback(GLFWwindow *wwindow, int fbW, int fbH);
    void initWindow();
    void initImGui();
    void initGlew();
    void initOpenGLOptions();

    void initShaders();
    void initCamera();
    void initMatrices();

    //Initialization for compute shader
    void initFrame();

    //Object in the scene initialization
    void initSkySphere();
    void initBlackHole();
    void initComputeUniforms();

    //variables update
    void updateDeltaTime();
    void updateImGui();
    void updateKeyboardInput();
    void updateMouseInput();
    void updateMatrices();
    void updateFrame();
    void updateComputeUniforms();
    void runComputeShader();
    void updateTextureBindings();
    void updateCoreUniforms();

    GLFWwindow *getWindow() const;

private:

    //Window
    GLFWwindow *window;
    const int W_WIDTH;
    const int W_HEIGHT;
    int fbwidth;
    int fbheight;
    const char* title;

    //Delta time
    float dt;
    float curTime;
    float lastTime;

    //Mouse variables
    double lastMouseX;
    double lastMouseY;
    double mouseX;
    double mouseY;
    double mouseOffsetX;
    double mouseOffsetY;
    bool firstMouse;

    //Shaders
    Shader *mainShader;
    ComputeShader *computeShader;
    std::vector<const char*> shaders = {"../shaders/compute.glsl",
                                        "../shaders/compute_doublep.glsl"};
    int current_shader = 0;
    int previous_shader = current_shader;

    //Camera and matrices
    Camera camera;
    glm::mat4 ViewMatrix;
    glm::mat4 ModelMatrix;
    glm::mat4 ProjectionMatrix;
    float nearPlane;

    //Frame
    Model* frame;
    Frame frameQuad;

    //SkySphere
    SkySphere *skySphere;
    //Selected background
    int selected_background = 0;
    int previous_background = selected_background;


    //Black Hole
    BlackHole *blackHole;

    //Others
    Texture *logo;
};


#endif //BLACK_HOLE_ENGINE_HPP
